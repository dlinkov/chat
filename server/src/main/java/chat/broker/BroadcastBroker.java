package chat.broker;

import chat.message.Message;
import chat.message.TextMessage;
import chat.store.MessageStore;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class BroadcastBroker implements Broker {

    private final int historyLimitOnJoin;
    private final MessageStore messageStore;

    private final List<Consumer> consumers = new ArrayList<>();
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public BroadcastBroker(int historyLimitOnJoin, MessageStore messageStore) {
        this.historyLimitOnJoin = historyLimitOnJoin;
        this.messageStore = messageStore;
    }

    @Override
    public void addConsumer(Consumer consumer) {
        lock.writeLock().lock();
        try {
            consumers.add(consumer);

            // TODO отправлять одним сообщением?
            List<Message> lastMessages = messageStore.getAllSortByTimestamp(historyLimitOnJoin);
            for (Message lastMessage : lastMessages) {
                consumer.sendMessage(lastMessage);
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public void removeConsumer(Consumer consumer) {
        lock.writeLock().lock();
        try {
            consumers.remove(consumer);
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public void sendMessage(Message message, Consumer consumer) {
        lock.readLock().lock();
        try {
            // TODO isPersistent
            if (message instanceof TextMessage) {
                messageStore.add(message);
            }

            for (Consumer cons : consumers) {
                if (cons.isAcceptable(message)) {
                    cons.sendMessage(message);
                }
            }
        } finally {
            lock.readLock().unlock();
        }
    }
}
