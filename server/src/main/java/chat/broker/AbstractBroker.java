package chat.broker;

import chat.message.Message;
import lombok.NonNull;

public abstract class AbstractBroker implements Broker {

    private final Broker nextBroker;

    AbstractBroker(@NonNull Broker nextBroker) {
        this.nextBroker = nextBroker;
    }

    abstract Message doSendMessage(Message message, Consumer consumer);

    @Override
    public void addConsumer(Consumer consumer) {
        nextBroker.addConsumer(consumer);
    }

    @Override
    public void removeConsumer(Consumer consumer) {
        nextBroker.removeConsumer(consumer);
    }

    @Override
    public void sendMessage(Message message, Consumer consumer) {
        Message response = doSendMessage(message, consumer);
        if (response != null) {
            nextBroker.sendMessage(response, consumer);
        }
    }
}
