package chat.broker;

import chat.message.Message;

public interface Broker {

    void addConsumer(Consumer consumer);

    void removeConsumer(Consumer consumer);

    void sendMessage(Message message, Consumer consumer);
}
