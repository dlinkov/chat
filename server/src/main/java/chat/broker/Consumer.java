package chat.broker;

import chat.transport.MessageListener;
import chat.Service;
import chat.User;
import chat.message.Message;
import chat.transport.Transport;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Consumer implements MessageListener, Service {

    private final Transport transport;
    private final Broker broker;

    @Getter
    @Setter
    private User user;

    public Consumer(Transport transport, Broker broker) {
        this.transport = transport;
        this.transport.setMessageListener(this);
        this.broker = broker;
    }

    @Override
    public void onMessage(Message message) {
        log.info("{} onMessage: {}", this, message);

        message.setUser(user);

        broker.sendMessage(message, this);
    }

    @Override
    public void onError(Exception e) {
        log.error("{} error occurred", this, e);
        try {
            stop();
        } catch (Exception ex) {
            log.error("Error stopping {}", this, ex);
        }
    }

    @Override
    public void start() throws Exception {
        log.debug("Starting {}", this);
        transport.start();
        broker.addConsumer(this);
    }

    @Override
    public void stop() throws Exception {
        log.debug("Stopping {}", this);
        broker.removeConsumer(this);
        transport.stop();
    }

    public boolean isAcceptable(Message message) {
        return message.getTargetUser() == null || message.getTargetUser().equals(user);
    }

    // TODO вынести в интерфейс и передавать его в брокер
    public void sendMessage(Message message) {
        log.info("{} sendMessage: {}", this, message);
        // TODO Отправлять через Executor
        transport.request(message);
    }

    @Override
    public String toString() {
        return "Consumer[" + transport.getRemoteAddress() + "]";
    }
}
