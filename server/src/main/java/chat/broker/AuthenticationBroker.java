package chat.broker;

import chat.User;
import chat.message.Message;
import chat.message.MessageFactory;
import chat.store.DuplicateUsernameException;
import chat.store.UserStore;
import chat.validator.UsernameValidator;
import chat.validator.ValidationException;

import java.util.ArrayList;
import java.util.List;

public class AuthenticationBroker extends AbstractBroker {

    private final UserStore userStore;
    private final UsernameValidator usernameValidator;
    private final MessageFactory messageFactory;

    private final List<User> authenticatedUsers = new ArrayList<>();

    public AuthenticationBroker(Broker nextBroker,
                                UserStore userStore,
                                UsernameValidator usernameValidator,
                                MessageFactory messageFactory) {
        super(nextBroker);
        this.userStore = userStore;
        this.usernameValidator = usernameValidator;
        this.messageFactory = messageFactory;
    }

    @Override
    public void addConsumer(Consumer consumer) {
        consumer.sendMessage(messageFactory.createSystemMessage("Enter your username"));
    }

    @Override
    public void removeConsumer(Consumer consumer) {
        synchronized (authenticatedUsers) {
            authenticatedUsers.remove(consumer.getUser());
        }
        super.removeConsumer(consumer);
    }

    @Override
    Message doSendMessage(Message message, Consumer consumer) {
        if (consumer.getUser() != null) {
            return message;
        }

        authenticate(message, consumer);

        return null;
    }

    private void authenticate(Message message, Consumer consumer) {
        try {
            String username = message.getText();

            usernameValidator.validate(username);

            // TODO factory
            User user = new User();
            user.setName(username);

            synchronized (authenticatedUsers) {
                if (authenticatedUsers.contains(user)) {
                    throw new DuplicateUsernameException(username);
                } else {
                    user = userStore.getOrAdd(user);
                    authenticatedUsers.add(user);
                    consumer.setUser(user);
                }
            }

            super.addConsumer(consumer);
        } catch (ValidationException | DuplicateUsernameException e) {
            consumer.sendMessage(messageFactory.createSystemMessage(e.getMessage()));
        }
    }
}
