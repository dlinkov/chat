package chat.broker;

import chat.message.CommandMessage;
import chat.message.Message;
import chat.message.MessageFactory;
import chat.command.*;
import chat.command.converter.TypeConverterFactory;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

@Slf4j
public class CommandBroker extends AbstractBroker {

    private final CommandMethodRegistry commandMethodRegistry;
    private final TypeConverterFactory typeConverterFactory;
    private final MessageFactory messageFactory;

    public CommandBroker(Broker nextBroker,
                         CommandMethodRegistry commandMethodRegistry,
                         TypeConverterFactory typeConverterFactory,
                         MessageFactory messageFactory) {
        super(nextBroker);
        this.commandMethodRegistry = commandMethodRegistry;
        this.typeConverterFactory = typeConverterFactory;
        this.messageFactory = messageFactory;
    }

    @Override
    Message doSendMessage(Message message, Consumer consumer) {
        if (!(message instanceof CommandMessage)) {
            return message;
        }

        CommandMessage commandMessage = (CommandMessage) message;

        try {
            return (Message) commandMethodRegistry.getCommandMethod(commandMessage.getCommandName())
                    .execute(commandMessage, typeConverterFactory);
        } catch (CommandException e) {
            return messageFactory.createSystemMessage(e.getMessage(), commandMessage.getUser());
        } catch (Exception e) {
            log.error("Failed execute command message: {}", commandMessage, e);
            return messageFactory.createSystemMessage("Internal error occurred", commandMessage.getUser());
        }
    }

    public void addCommand(Command command) {
        for (Method method : command.getClass().getDeclaredMethods()) {
            CmdMethod cmdMethod = method.getAnnotation(CmdMethod.class);
            if (cmdMethod == null) {
                continue;
            }

            commandMethodRegistry.register(
                    commandMethodRegistry.createCommandMethod(command, method, typeConverterFactory));
        }
    }
}
