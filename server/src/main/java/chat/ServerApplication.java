package chat;

import chat.broker.*;
import chat.command.CommandMethodRegistry;
import chat.command.HelpCommand;
import chat.command.RenameCommand;
import chat.command.converter.TypeConverterFactory;
import chat.message.MessageFactory;
import chat.store.InMemoryMessageStore;
import chat.store.InMemoryUserStore;
import chat.transport.ServerTransport;
import chat.transport.Transport;
import chat.transport.TransportAcceptListener;
import chat.transport.TransportFactory;
import chat.transport.format.JsonMessageFormatter;
import chat.transport.format.MessageFormatter;
import chat.transport.nio.NIOServerSocketFactory;
import chat.transport.nio.NIOTransportFactory;
import chat.transport.nio.SelectorFactory;
import chat.validator.UsernameValidator;
import lombok.extern.slf4j.Slf4j;

import javax.net.ServerSocketFactory;

@Slf4j
public class ServerApplication implements TransportAcceptListener, Service {

    private final ServerTransport serverTransport;
    private final Broker broker;

    public ServerApplication(ServerTransport serverTransport, Broker broker) {
        this.serverTransport = serverTransport;
        this.serverTransport.setTransportAcceptListener(this);
        this.broker = broker;
    }

    @Override
    public void onAccept(Transport transport) {
        // TODO factory
        Consumer consumer = new Consumer(transport, broker);
        try {
            consumer.start();
        } catch (Exception e) {
            onAcceptError(e);
        }
    }

    @Override
    public void onAcceptError(Exception e) {
        log.error("onAcceptError: " + e.getMessage(), e);
    }

    @Override
    public void start() throws Exception {
        serverTransport.start();
    }

    @Override
    public void stop() throws Exception {
        // TODO stop
    }

    public static void main(String[] args) throws Exception {
        int port = 90;
        int backlog = 50;

        InMemoryUserStore userStore = new InMemoryUserStore();
        InMemoryMessageStore messageStore = new InMemoryMessageStore(200);
        MessageFactory messageFactory = new MessageFactory();
        MessageFormatter messageFormatter = new JsonMessageFormatter();
        UsernameValidator usernameValidator = new UsernameValidator();

        // Transports
//        ServerSocketFactory serverSocketFactory = new TcpServerSocketFactory();
//        TransportFactory transportFactory = new TcpTransportFactory(serverSocketFactory, messageFormatter);
        ServerSocketFactory serverSocketFactory = new NIOServerSocketFactory();
        SelectorFactory selectorFactory = new SelectorFactory();
        TransportFactory transportFactory = new NIOTransportFactory(serverSocketFactory, selectorFactory,
                messageFormatter);

        ServerTransport serverTransport = transportFactory.createServerTransport(port, backlog);

        // Brokers
        Broker broker = new BroadcastBroker(100, messageStore);

        TypeConverterFactory typeConverterFactory = new TypeConverterFactory();
        CommandMethodRegistry commandMethodRegistry = new CommandMethodRegistry();
        CommandBroker commandBroker = new CommandBroker(broker, commandMethodRegistry, typeConverterFactory,
                messageFactory);
        commandBroker.addCommand(new HelpCommand(commandMethodRegistry, messageFactory));
        commandBroker.addCommand(new RenameCommand(userStore, usernameValidator, messageFactory));

        broker = commandBroker;
        broker = new AuthenticationBroker(broker, userStore, usernameValidator, messageFactory);

        // App
        ServerApplication chatServer = new ServerApplication(serverTransport, broker);
        chatServer.start();
    }
}
