package chat.validator;

import java.util.regex.Pattern;

public class UsernameValidator {

    private static final Pattern ALLOWED_CHARACTERS_PATTERN = Pattern.compile("[A-Za-z0-9_]+");

    public void validate(String username) {
        if (username.length() < 4 || username.length() > 25) {
            throw new ValidationException("Username must be between 4 and 25 characters");
        }

        if (!ALLOWED_CHARACTERS_PATTERN.matcher(username).matches()) {
            throw new ValidationException("Username must only contain alphanumeric characters");
        }
    }
}
