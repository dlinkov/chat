package chat.validator;

public class ValidationException extends RuntimeException {

    ValidationException(String message) {
        super(message);
    }
}
