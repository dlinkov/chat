package chat.command;

import chat.message.Message;
import chat.command.converter.TypeConverterFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

public class CommandMethodRegistry {

    private final Map<String, CommandMethod> commandMethodByName = new HashMap<>();

    public CommandMethod getCommandMethod(String commandName) {
        CommandMethod commandMethod = commandMethodByName.get(commandName);
        if (commandMethod == null) {
            throw new CommandException(String.format("Unrecognized command: '%s'", commandName));
        }

        return commandMethod;
    }

    public Collection<CommandMethod> getCommandMethods() {
        return commandMethodByName.values();
    }

    public void register(CommandMethod commandMethod) {
        String name = commandMethod.getName();

        CommandMethod otherCommandMethod = commandMethodByName.get(name);
        if (otherCommandMethod != null) {
            throw new IllegalArgumentException(
                    String.format("Ambiguous commands by name: '%s', found: [%s, %s]", name, commandMethod,
                            otherCommandMethod));
        }

        commandMethodByName.put(name, commandMethod);
    }

    public CommandMethod createCommandMethod(Command command, Method method,
                                             TypeConverterFactory typeConverterFactory) {
        return new CommandMethod(command, method,
                createCommandParameters(method.getParameters(), typeConverterFactory));
    }

    private List<CommandParameter> createCommandParameters(Parameter[] parameters,
                                                           TypeConverterFactory typeConverterFactory) {
        ArrayList<CommandParameter> result = new ArrayList<>(parameters.length);

        int index = 0;
        for (Parameter parameter : parameters) {
            if (!isProvidedType(parameter)) {
                typeConverterFactory.getTypeConverter(parameter.getType());
            }

            result.add(createCommandParameter(parameter,
                    isProvidedType(parameter) ? CommandParameter.PROVIDED_INDEX : index++));
        }

        return result;
    }

    private CommandParameter createCommandParameter(Parameter parameter, int index) {
        return new CommandParameter(parameter, index);
    }

    private boolean isProvidedType(Parameter parameter) {
        return Message.class.isAssignableFrom(parameter.getType());
    }
}