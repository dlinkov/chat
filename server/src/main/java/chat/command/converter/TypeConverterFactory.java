package chat.command.converter;

import java.util.Arrays;
import java.util.List;

public class TypeConverterFactory {

    // TODO addConverter
    private final List<TypeConverter<?>> typeConverters = Arrays.asList(
            new StringTypeConverter(),
            new IntegerTypeConverter(),
            new LongTypeConverter(),
            new FloatTypeConverter(),
            new DoubleTypeConverter(),
            new BooleanTypeConverter(),
            new EnumTypeConverter()
    );

    public <T> TypeConverter<T> getTypeConverter(Class<T> type) {
        for (TypeConverter<?> typeConverter : typeConverters) {
            if (typeConverter.canConvertTo(type)) {
                return (TypeConverter<T>) typeConverter;
            }
        }

        throw new IllegalArgumentException(String.format("Cannot find converter for type: '%s'", type));
    }
}
