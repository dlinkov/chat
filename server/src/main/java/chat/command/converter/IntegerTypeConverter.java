package chat.command.converter;

public class IntegerTypeConverter implements TypeConverter<Integer> {

    @Override
    public boolean canConvertTo(Class<?> targetType) {
        return Integer.class.isAssignableFrom(targetType) || int.class.isAssignableFrom(targetType);
    }

    @Override
    public Integer convertFromString(String value, Class<?> targetType) {
        return Integer.parseInt(value);
    }
}
