package chat.command.converter;

public class DoubleTypeConverter implements TypeConverter<Double> {

    @Override
    public boolean canConvertTo(Class<?> targetType) {
        return Double.class.isAssignableFrom(targetType) || double.class.isAssignableFrom(targetType);
    }

    @Override
    public Double convertFromString(String value, Class<?> targetType) {
        return Double.parseDouble(value);
    }
}
