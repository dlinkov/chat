package chat.command.converter;

public class FloatTypeConverter implements TypeConverter<Float> {

    @Override
    public boolean canConvertTo(Class<?> targetType) {
        return Float.class.isAssignableFrom(targetType) || float.class.isAssignableFrom(targetType);
    }

    @Override
    public Float convertFromString(String value, Class<?> targetType) {
        return Float.parseFloat(value);
    }
}
