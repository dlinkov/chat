package chat.command.converter;

public class LongTypeConverter implements TypeConverter<Long> {

    @Override
    public boolean canConvertTo(Class<?> targetType) {
        return Long.class.isAssignableFrom(targetType) || long.class.isAssignableFrom(targetType);
    }

    @Override
    public Long convertFromString(String value, Class<?> targetType) {
        return Long.parseLong(value);
    }
}
