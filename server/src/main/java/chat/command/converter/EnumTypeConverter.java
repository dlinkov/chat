package chat.command.converter;

public class EnumTypeConverter implements TypeConverter<Enum<?>> {

    @Override
    public boolean canConvertTo(Class<?> targetType) {
        return Enum.class.isAssignableFrom(targetType);
    }

    @Override
    public Enum<?> convertFromString(String value, Class<?> targetType) {
        return Enum.valueOf((Class<Enum>) targetType, value);
    }
}
