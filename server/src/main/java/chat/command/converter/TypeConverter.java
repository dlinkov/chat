package chat.command.converter;

public interface TypeConverter<T> {

    boolean canConvertTo(Class<?> targetType);

    T convertFromString(String value, Class<?> targetType);
}
