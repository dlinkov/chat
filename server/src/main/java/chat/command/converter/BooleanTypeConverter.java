package chat.command.converter;

public class BooleanTypeConverter implements TypeConverter<Boolean> {

    @Override
    public boolean canConvertTo(Class<?> targetType) {
        return Boolean.class.isAssignableFrom(targetType) || boolean.class.isAssignableFrom(targetType);
    }

    @Override
    public Boolean convertFromString(String value, Class<?> targetType) {
        if ("true".equalsIgnoreCase(value)) {
            return Boolean.TRUE;
        }
        if ("false".equalsIgnoreCase(value)) {
            return Boolean.FALSE;
        }

        throw new IllegalArgumentException("Incorrect boolean value");
    }
}
