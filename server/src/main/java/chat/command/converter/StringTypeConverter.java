package chat.command.converter;

public class StringTypeConverter implements TypeConverter<String> {

    @Override
    public boolean canConvertTo(Class<?> targetType) {
        return String.class.isAssignableFrom(targetType);
    }

    @Override
    public String convertFromString(String value, Class<?> targetType) {
        return value;
    }
}
