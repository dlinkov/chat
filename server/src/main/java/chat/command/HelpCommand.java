package chat.command;

import chat.message.Message;
import chat.message.MessageFactory;

import java.util.Comparator;

public class HelpCommand implements Command {

    private static final int DESCRIPTION_INDENT_SIZE = 15;

    private final CommandMethodRegistry commandMethodRegistry;
    private final MessageFactory messageFactory;

    public HelpCommand(CommandMethodRegistry commandMethodRegistry, MessageFactory messageFactory) {
        this.commandMethodRegistry = commandMethodRegistry;
        this.messageFactory = messageFactory;
    }

    @CmdMethod(description = "List available commands. Type 'help <command>' to learn more")
    public Message help(
            @CmdParameter(name = "command",
                    description = "Command name to obtain help for", required = false) String commandName,
            Message message) {
        String result;
        if (commandName != null) {
            result = getCommandHelp(commandName);
        } else {
            result = getCommandsHelp();
        }

        return messageFactory.createSystemMessage(result, message.getUser());
    }

    private String getCommandHelp(String commandName) {
        CommandMethod commandMethod = commandMethodRegistry.getCommandMethod(commandName);

        StringBuilder result = new StringBuilder(commandMethod.getName());

        for (CommandParameter commandParameter : commandMethod.getCommandParameters()) {
            if (commandParameter.isProvided()) {
                continue;
            }

            result.append(" ");
            if (commandParameter.isRequired()) {
                result.append("<");
                result.append(commandParameter.getName());
                result.append(">");
            } else {
                result.append("[<");
                result.append(commandParameter.getName());
                result.append(">]");
            }
        }

        if (!commandMethod.getDescription().isEmpty()) {
            result.append("\n  ");
            result.append(commandMethod.getDescription());
        }

        if (!commandMethod.getCommandParameters().isEmpty()) {
            result.append("\n\n");
            result.append("  Arguments:");

            for (CommandParameter commandParameter : commandMethod.getCommandParameters()) {
                if (commandParameter.isProvided()) {
                    continue;
                }

                result.append("\n    <");
                result.append(commandParameter.getName());
                result.append(">");
                appendIndent(result, DESCRIPTION_INDENT_SIZE - commandParameter.getName().length());
                result.append(commandParameter.getDescription());

                if (!commandParameter.isRequired()) {
                    result.append(" (Optional)");
                }
            }
        }

        return result.toString();
    }

    private String getCommandsHelp() {
        StringBuilder result = new StringBuilder();

        commandMethodRegistry.getCommandMethods().stream()
                .sorted(Comparator.comparing(CommandMethod::getName))
                .forEach(commandMethod -> {
                    result.append(commandMethod.getName());
                    appendIndent(result, DESCRIPTION_INDENT_SIZE - commandMethod.getName().length());
                    result.append(commandMethod.getDescription())
                            .append('\n');
                });

        return result.toString();
    }

    private void appendIndent(StringBuilder sb, int size) {
        if (size < 0) {
            size = 1;
        }

        for (int i = 0; i < size; i++) {
            sb.append(" ");
        }
    }
}