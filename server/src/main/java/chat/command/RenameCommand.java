package chat.command;

import chat.store.DuplicateUsernameException;
import chat.message.Message;
import chat.message.MessageFactory;
import chat.store.UserStore;
import chat.validator.UsernameValidator;
import chat.validator.ValidationException;

public class RenameCommand implements Command {

    private final UserStore userStore;
    private final UsernameValidator usernameValidator;
    private final MessageFactory messageFactory;

    public RenameCommand(UserStore userStore,
                         UsernameValidator usernameValidator,
                         MessageFactory messageFactory) {
        this.userStore = userStore;
        this.usernameValidator = usernameValidator;
        this.messageFactory = messageFactory;
    }

    @CmdMethod
    public Message rename(String username, Message message) {
        if (message.getUser() == null || username.equals(message.getUser().getName())) {
            return null;
        }

        try {
            usernameValidator.validate(username);
            userStore.updateName(message.getUser(), username);
        } catch (ValidationException | DuplicateUsernameException e) {
            throw new CommandException(e.getMessage(), e);
        }

        return messageFactory.createSystemMessage("Username successfully changed", message.getUser());
    }
}
