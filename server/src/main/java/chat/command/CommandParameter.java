package chat.command;

import chat.command.converter.TypeConverter;
import chat.command.converter.TypeConverterFactory;

import java.lang.reflect.Parameter;
import java.util.List;

public class CommandParameter {

    public static final int PROVIDED_INDEX = -1;

    private final Parameter parameter;
    private final int index;

    CommandParameter(Parameter parameter, int index) {
        this.parameter = parameter;
        this.index = index;
    }

    public Object resolveProvidedArgument(Object... providedArgs) {
        if (providedArgs == null) {
            return null;
        }

        for (Object resolvedArg : providedArgs) {
            if (parameter.getType().isInstance(resolvedArg)) {
                return resolvedArg;
            }
        }

        return null;
    }

    public Object resolveArgument(List<String> rawArgs, TypeConverterFactory typeConverterFactory) {
        String value = "";
        if (rawArgs.size() > index) {
            value = rawArgs.get(index);
        }

        String name = getName();
        if (value.isEmpty()) {
            if (isRequired()) {
                throw new CommandException(String.format("Parameter '%s' is required", name));
            } else {
                return null;
            }
        }

        TypeConverter<?> typeConverter = typeConverterFactory.getTypeConverter(parameter.getType());
        try {
            return typeConverter.convertFromString(value, parameter.getType());
        } catch (RuntimeException e) {
            throw new CommandException(String.format("Parameter '%s' has incorrect value: '%s'", name, value), e);
        }
    }

    public String getName() {
        CmdParameter cmdParameter = parameter.getAnnotation(CmdParameter.class);
        if (cmdParameter != null && cmdParameter.name().length() > 0) {
            return cmdParameter.name();
        }

        return parameter.getName();
    }

    public String getDescription() {
        CmdParameter cmdParameter = parameter.getAnnotation(CmdParameter.class);
        if (cmdParameter != null && cmdParameter.description().length() > 0) {
            return cmdParameter.description();
        }

        return "";
    }

    public boolean isRequired() {
        if (parameter.getType().isPrimitive()) {
            return true;
        }

        CmdParameter cmdParameter = parameter.getAnnotation(CmdParameter.class);

        return cmdParameter == null || cmdParameter.required();
    }

    public boolean isProvided() {
        return index == PROVIDED_INDEX;
    }
}
