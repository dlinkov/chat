package chat.command;

import chat.message.CommandMessage;
import chat.command.converter.TypeConverterFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

public class CommandMethod {

    private final Command command;
    private final Method method;
    private final List<CommandParameter> commandParameters;

    CommandMethod(Command command, Method method, List<CommandParameter> commandParameters) {
        Objects.requireNonNull(method.getAnnotation(CmdMethod.class), "CmdMethod not found");
        this.command = command;
        this.method = method;
        this.commandParameters = commandParameters;
    }

    public Object execute(CommandMessage commandMessage, TypeConverterFactory typeConverterFactory) {
        List<String> rawArgs = commandMessage.getRawArgs();

        Object[] args = new Object[commandParameters.size()];
        for (int i = 0; i < commandParameters.size(); i++) {
            CommandParameter commandParameter = commandParameters.get(i);
            if (commandParameter.isProvided()) {
                args[i] = commandParameter.resolveProvidedArgument(commandMessage);
            } else {
                args[i] = commandParameter.resolveArgument(rawArgs, typeConverterFactory);
            }
        }

        // TODO Too many arguments

        try {
            return method.invoke(command, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            if (e.getCause() instanceof CommandException) {
                throw (CommandException) e.getCause();
            }
            throw new RuntimeException("Failed to invoke command method", e);
        }
    }

    public String getName() {
        CmdMethod cmdMethod = method.getAnnotation(CmdMethod.class);
        if (cmdMethod.name().length() > 0) {
            return cmdMethod.name();
        }

        return method.getName();
    }

    public String getDescription() {
        return method.getAnnotation(CmdMethod.class).description();
    }

    public List<CommandParameter> getCommandParameters() {
        return commandParameters;
    }

    @Override
    public String toString() {
        return method.toString();
    }
}
