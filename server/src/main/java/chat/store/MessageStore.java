package chat.store;

import chat.message.Message;

import java.util.List;

public interface MessageStore {

    void add(Message message);

    List<Message> getAllSortByTimestamp(int limit);
}
