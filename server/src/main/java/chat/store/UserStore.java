package chat.store;

import chat.User;

public interface UserStore {

    User getOrAdd(User user);

    void updateName(User user, String name) throws DuplicateUsernameException;
}
