package chat.store;

import chat.User;

import java.util.*;

public class InMemoryUserStore implements UserStore {

    private final List<User> users = new ArrayList<>();
    private long sequenceId;

    @Override
    public synchronized User getOrAdd(User user) {
        for (User existedUser : users) {
            if (existedUser.equals(user)) {
                return existedUser;
            }
        }

        user.setId(++sequenceId);
        users.add(user);

        return user;
    }

    @Override
    public synchronized void updateName(User user, String name) {
        for (User existedUser : users) {
            if (Objects.equals(existedUser.getName(), name)) {
                throw new DuplicateUsernameException(name);
            }
        }

        user.setName(name);
    }
}