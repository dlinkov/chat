package chat.store;

import chat.message.Message;
import chat.util.SizedMap;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryMessageStore implements MessageStore {

    private final Map<Long, Message> messageById;
    private long sequenceId;

    public InMemoryMessageStore(int maxSize) {
        this.messageById = new SizedMap<>(maxSize);
    }

    @Override
    public synchronized void add(Message message) {
        message.setId(++sequenceId);
        messageById.put(message.getId(), message);
    }

    @Override
    public synchronized List<Message> getAllSortByTimestamp(int limit) {
        int skip = messageById.size() - limit;
        if (skip < 0) {
            skip = 0;
        }

        return messageById.values().stream()
                .sorted(Comparator.comparing(Message::getTimestamp))
                .skip(skip)
                .limit(limit)
                .collect(Collectors.toList());
    }
}
