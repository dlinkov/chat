package chat.store;

public class DuplicateUsernameException extends RuntimeException {

    public DuplicateUsernameException(String username) {
        super(String.format("Username '%s' is already taken", username));
    }
}
