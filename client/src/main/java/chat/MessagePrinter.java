package chat;

import chat.message.Message;
import chat.message.TextMessage;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class MessagePrinter {

    public void print(Message message) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(message.getTimestamp()),
                ZoneId.systemDefault());

        StringBuilder sb = new StringBuilder();

        if (message instanceof TextMessage) {
            sb.append(dateTime.format(dateTimeFormatter));
            sb.append(" ");
            sb.append(message.getUserName());
            sb.append(": ");
        }
        sb.append(message.getText());

        System.out.println(sb.toString());
    }
}
