package chat;

import chat.message.Message;
import chat.message.MessageFactory;
import chat.transport.MessageListener;
import chat.transport.Transport;
import chat.transport.format.JsonMessageFormatter;
import chat.transport.format.MessageFormatter;
import chat.transport.tcp.TcpServerSocketFactory;
import chat.transport.tcp.TcpTransportFactory;

import java.net.Socket;
import java.util.Scanner;

public class ClientApplication implements MessageListener, Service {

    private final Transport transport;
    private final MessageFactory messageFactory;
    private final MessagePrinter messagePrinter;

    public ClientApplication(Transport transport, MessageFactory messageFactory, MessagePrinter messagePrinter) {
        this.transport = transport;
        this.messagePrinter = messagePrinter;
        this.transport.setMessageListener(this);
        this.messageFactory = messageFactory;
    }

    @Override
    public void onMessage(Message message) {
        messagePrinter.print(message);
    }

    @Override
    public void onError(Exception exception) {
        // TODO stop?
        exception.printStackTrace();
    }

    @Override
    public void start() throws Exception {
        transport.start();

        Scanner scanner = new Scanner(System.in);
        // TODO isRunning
        while (true) {
            sendMessage(messageFactory.createMessage(scanner.nextLine()));
        }
    }

    @Override
    public void stop() throws Exception {
        // TODO stop
    }

    private void sendMessage(Message message) {
        transport.request(message);
    }

    public static void main(String[] args) throws Exception {
        String host = "localhost";
        int port = 90;

        MessageFactory messageFactory = new MessageFactory();
        MessageFormatter messageFormatter = new JsonMessageFormatter();
        MessagePrinter messagePrinter = new MessagePrinter();

        TcpTransportFactory transportFactory = new TcpTransportFactory(new TcpServerSocketFactory(),
                messageFormatter);
        Transport transport = transportFactory.createTransport(new Socket(host, port));

        ClientApplication clientApplication = new ClientApplication(transport, messageFactory, messagePrinter);
        clientApplication.start();
    }
}