package chat.message;

import lombok.Getter;

@Getter
public class SystemMessage extends Message {

    private final String type = Type.SYSTEM;
}
