package chat.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
public class CommandMessage extends Message {

    public static final String COMMAND_PREFIX = "/";

    private final String type = Type.COMMAND;
    private final String commandName;
    private final List<String> rawArgs;

    @JsonCreator
    CommandMessage(@JsonProperty("text") String text) {
        if (text == null || !text.startsWith(COMMAND_PREFIX)) {
            throw new IllegalArgumentException(String.format("Command must start with '%s'", COMMAND_PREFIX));
        }

        setText(text);

        String[] parts = text.split("\\s+");

        this.commandName = parts[0].substring(1);
        if (parts.length >= 2) {
            this.rawArgs = Arrays.asList(parts).subList(1, parts.length);
        } else {
            this.rawArgs = Collections.emptyList();
        }
    }
}
