package chat.message;

import chat.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type",
        visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = TextMessage.class, name = Message.Type.TEXT),
        @JsonSubTypes.Type(value = SystemMessage.class, name = Message.Type.SYSTEM),
        @JsonSubTypes.Type(value = CommandMessage.class, name = Message.Type.COMMAND),
})
public abstract class Message {

    static final class Type {
        public static final String TEXT = "text";
        public static final String SYSTEM = "system";
        public static final String COMMAND = "command";

        private Type() {
        }
    }

    private Long id;
    private String type;
    private String text;
    private long timestamp;

    private User user;
    private User targetUser;

    @JsonIgnore
    public String getUserName() {
        return user != null ? user.getName() : null;
    }
}
