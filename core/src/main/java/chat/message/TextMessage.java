package chat.message;

import lombok.Getter;

@Getter
public class TextMessage extends Message {

    private final String type = Type.TEXT;
}
