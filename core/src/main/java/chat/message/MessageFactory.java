package chat.message;

import chat.User;

public class MessageFactory {

    public Message createMessage(String text) {
        if (text != null && text.startsWith(CommandMessage.COMMAND_PREFIX)) {
            return createCommandMessage(text);
        } else {
            return createTextMessage(text);
        }
    }

    public TextMessage createTextMessage(String text) {
        TextMessage textMessage = new TextMessage();
        textMessage.setText(text);
        textMessage.setTimestamp(System.currentTimeMillis());

        return textMessage;
    }

    public CommandMessage createCommandMessage(String text) {
        CommandMessage commandMessage = new CommandMessage(text);
        commandMessage.setTimestamp(System.currentTimeMillis());

        return commandMessage;
    }

    public SystemMessage createSystemMessage(String text) {
        SystemMessage systemMessage = new SystemMessage();
        systemMessage.setText(text);
        systemMessage.setTimestamp(System.currentTimeMillis());

        return systemMessage;
    }

    public SystemMessage createSystemMessage(String text, User targetUser) {
        SystemMessage systemMessage = createSystemMessage(text);
        systemMessage.setTargetUser(targetUser);

        return systemMessage;
    }
}
