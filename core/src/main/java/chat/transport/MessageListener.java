package chat.transport;

import chat.message.Message;

public interface MessageListener {

    void onMessage(Message message);

    void onError(Exception e);
}
