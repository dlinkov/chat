package chat.transport.nio;

import chat.transport.MessageListener;
import chat.message.Message;
import chat.transport.format.MessageFormatter;
import chat.transport.Transport;
import lombok.extern.slf4j.Slf4j;

import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

@Slf4j
public class NIOTransport implements Transport {

    private final Selector selector;
    private final SocketChannel socketChannel;
    private final MessageFormatter messageFormatter;
    // TODO вынести в настройки
    private final ByteBuffer readBuffer = ByteBuffer.allocate(256);

    private MessageListener messageListener;

    NIOTransport(Selector selector, SocketChannel socketChannel, MessageFormatter messageFormatter) {
        this.selector = selector;
        this.messageFormatter = messageFormatter;
        this.socketChannel = socketChannel;
    }

    @Override
    public String getRemoteAddress() {
        try {
            return "" + socketChannel.getRemoteAddress();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void request(Message message) {
        try {
            // TODO Сделать переиспользуемым
            ByteBuffer buffer = ByteBuffer.wrap(messageFormatter.serialize(message));
            // TODO если вернёт 0?
            socketChannel.write(buffer);
        } catch (IOException e) {
            // TODO Ловить в consumer?
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setMessageListener(MessageListener messageListener) {
        this.messageListener = messageListener;
    }

    private void readMessage() {
        try {
            StringBuilder sb = new StringBuilder();

            while (true) {
                int bytesRead = socketChannel.read(readBuffer);
                if (bytesRead == -1) {
                    messageListener.onError(new EOFException());
                    socketChannel.close();
                    break;
                }
                if (bytesRead == 0) {
                    break;
                }

                readBuffer.flip();
                while (readBuffer.hasRemaining()) {
                    byte b = readBuffer.get();
                    // TODO Убрать это. Изменить формат передачи сообщений на length + body
                    if (b == '\n') {
                        Message message = messageFormatter.deserialize(sb.toString().getBytes());

                        messageListener.onMessage(message);

                        sb = new StringBuilder();
                    } else {
                        sb.append((char) b);
                    }
                }

                readBuffer.clear();
            }
        } catch (IOException e) {
            try {
                socketChannel.close();
            } catch (IOException ex) {
                log.debug("Error closing SocketChannel: {}", socketChannel, e);
            }
            messageListener.onError(e);
        }
    }

    @Override
    public void start() throws Exception {
        socketChannel.configureBlocking(false);
        socketChannel.register(selector, SelectionKey.OP_READ,
                (NIOServerTransport.SelectionKeyListener) NIOTransport.this::readMessage);
    }

    @Override
    public void stop() throws Exception {
        // TODO stop
    }
}
