package chat.transport.nio;

import chat.transport.ServerTransport;
import chat.transport.format.MessageFormatter;
import chat.transport.Transport;
import chat.transport.TransportFactory;

import javax.net.ServerSocketFactory;
import java.net.Socket;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public class NIOTransportFactory implements TransportFactory {

    private final ServerSocketFactory serverSocketFactory;
    private final SelectorFactory selectorFactory;
    private final MessageFormatter messageFormatter;

    public NIOTransportFactory(ServerSocketFactory serverSocketFactory,
                               SelectorFactory selectorFactory,
                               MessageFormatter messageFormatter) {
        this.serverSocketFactory = serverSocketFactory;
        this.selectorFactory = selectorFactory;
        this.messageFormatter = messageFormatter;
    }

    @Override
    public ServerTransport createServerTransport(int port, int backlog) {
        return new NIOServerTransport(port, backlog, serverSocketFactory, selectorFactory, this);
    }

    @Override
    public Transport createTransport(Socket socket) {
        throw new UnsupportedOperationException();
    }

    public NIOTransport createTransport(Selector selector, SocketChannel socketChannel) {
        return new NIOTransport(selector, socketChannel, messageFormatter);
    }
}
