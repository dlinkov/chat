package chat.transport.nio;

import javax.net.ServerSocketFactory;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.ServerSocketChannel;

public class NIOServerSocketFactory extends ServerSocketFactory {

    @Override
    public ServerSocket createServerSocket(int port) throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.socket().bind(new InetSocketAddress(port));
        return serverSocketChannel.socket();
    }

    @Override
    public ServerSocket createServerSocket(int port, int backlog) throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.socket().bind(new InetSocketAddress(port), backlog);
        return serverSocketChannel.socket();
    }

    @Override
    public ServerSocket createServerSocket(int port, int backlog, InetAddress inetAddress) throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.socket().bind(new InetSocketAddress(inetAddress, port), backlog);
        return serverSocketChannel.socket();
    }
}
