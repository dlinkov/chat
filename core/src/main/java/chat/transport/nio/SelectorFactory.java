package chat.transport.nio;

import java.io.IOException;
import java.nio.channels.Selector;

public class SelectorFactory {

    public Selector createSelector() throws IOException {
        return Selector.open();
    }
}
