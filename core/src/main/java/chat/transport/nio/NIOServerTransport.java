package chat.transport.nio;

import chat.transport.ServerTransport;
import chat.transport.Transport;
import chat.transport.TransportAcceptListener;
import lombok.extern.slf4j.Slf4j;

import javax.net.ServerSocketFactory;
import java.io.IOException;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class NIOServerTransport implements ServerTransport, Runnable {

    private final int port;
    private final int backlog;
    private final ServerSocketFactory serverSocketFactory;
    private final SelectorFactory selectorFactory;
    private final NIOTransportFactory transportFactory;
    // TODO вынести в настройки
    private final ExecutorService executor = Executors.newFixedThreadPool(4);

    private Selector selector;
    private TransportAcceptListener transportAcceptListener;

    NIOServerTransport(int port,
                       int backlog,
                       ServerSocketFactory serverSocketFactory,
                       SelectorFactory selectorFactory,
                       NIOTransportFactory transportFactory) {
        this.port = port;
        this.backlog = backlog;
        this.serverSocketFactory = serverSocketFactory;
        this.selectorFactory = selectorFactory;
        this.transportFactory = transportFactory;
    }

    public interface SelectionKeyListener {

        void onRead();
    }

    @Override
    public void setTransportAcceptListener(TransportAcceptListener transportAcceptListener) {
        this.transportAcceptListener = transportAcceptListener;
    }

    @Override
    public void start() throws Exception {
        log.info("Starting server...");
        new Thread(this).start();
    }

    @Override
    public void stop() throws Exception {
        // TODO stop
    }

    @Override
    public void run() {
        try {
            ServerSocketChannel serverSocketChannel = serverSocketFactory.createServerSocket(port, backlog)
                    .getChannel();
            serverSocketChannel.configureBlocking(false);

            Selector selector = selectorFactory.createSelector();

            try {
                serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            } catch (ClosedChannelException e) {
                try {
                    selector.close();
                } catch (IOException ignore) {
                }
                throw e;
            }

            this.selector = selector;

            log.debug("Server started on port: {}", port);

            // TODO isRunning()
            while (true) {
                int keyCount = selector.select(10);
                if (keyCount == 0) {
                    continue;
                }

                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while (iterator.hasNext()) {
                    SelectionKey selectionKey = iterator.next();
                    iterator.remove();

                    if (selectionKey.isAcceptable()) {
                        handleAccept(selectionKey);
                    }

                    if (selectionKey.isReadable()) {
                        handleRead(selectionKey);
                    }
                }
            }
        } catch (IOException e) {
            transportAcceptListener.onAcceptError(e);
        }
    }

    // TODO вынести в отдельный класс?
    private void handleAccept(SelectionKey selectionKey) {
        try {
            SocketChannel socketChannel = ((ServerSocketChannel) selectionKey.channel()).accept();

            log.debug("Handle accept SocketChannel: {}", socketChannel);

            Transport transport = transportFactory.createTransport(selector, socketChannel);

            transportAcceptListener.onAccept(transport);
        } catch (IOException e) {
            transportAcceptListener.onAcceptError(e);
        }
    }

    // TODO вынести в отдельный класс?
    private void handleRead(SelectionKey selectionKey) {
        log.debug("Handle read SocketChannel: {}", selectionKey.channel());
        if (selectionKey.isValid()) {
            selectionKey.interestOps(0);
        }

        SelectionKeyListener selectionKeyListener = (SelectionKeyListener) selectionKey.attachment();

        executor.execute(() -> {
            selectionKeyListener.onRead();
            if (selectionKey.isValid()) {
                selectionKey.interestOps(SelectionKey.OP_READ);
            }
        });
    }
}
