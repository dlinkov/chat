package chat.transport.tcp;

import chat.transport.MessageListener;
import chat.message.Message;
import chat.transport.format.MessageFormatter;
import chat.transport.Transport;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

@Slf4j
public class TcpTransport implements Transport, Runnable {

    private final Socket socket;
    private final MessageFormatter messageFormatter;

    private BufferedReader in;
    private OutputStream out;

    private MessageListener messageListener;

    TcpTransport(Socket socket, MessageFormatter messageFormatter) {
        this.socket = socket;
        this.messageFormatter = messageFormatter;
    }

    @Override
    public String getRemoteAddress() {
        return "" + socket.getRemoteSocketAddress();
    }

    @Override
    public void request(Message message) {
        try {
            out.write(messageFormatter.serialize(message));
            out.flush();
        } catch (IOException e) {
            // TODO Ловить в consumer?
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setMessageListener(MessageListener messageListener) {
        this.messageListener = messageListener;
    }

    @Override
    public void start() throws Exception {
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = socket.getOutputStream();

        new Thread(this).start();
    }

    @Override
    public void stop() throws Exception {
        in.close();
        out.close();
    }

    @Override
    public void run() {
        try {
            String line;
            // TODO Убрать это. Изменить формат передачи сообщений на length + body
            while ((line = in.readLine()) != null) {
                messageListener.onMessage(messageFormatter.deserialize(line.getBytes()));
            }
        } catch (Exception e) {
            messageListener.onError(e);
        }
    }
}
