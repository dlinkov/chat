package chat.transport.tcp;

import chat.transport.format.MessageFormatter;
import chat.transport.Transport;
import chat.transport.TransportFactory;
import chat.transport.ServerTransport;

import javax.net.ServerSocketFactory;
import java.net.Socket;

public class TcpTransportFactory implements TransportFactory {

    private final ServerSocketFactory serverSocketFactory;
    private final MessageFormatter messageFormatter;

    public TcpTransportFactory(ServerSocketFactory serverSocketFactory, MessageFormatter messageFormatter) {
        this.serverSocketFactory = serverSocketFactory;
        this.messageFormatter = messageFormatter;
    }

    @Override
    public ServerTransport createServerTransport(int port, int backlog) {
        return new TcpServerTransport(port, backlog, serverSocketFactory, this);
    }

    @Override
    public Transport createTransport(Socket socket) {
        return new TcpTransport(socket, messageFormatter);
    }
}
