package chat.transport.tcp;

import chat.transport.TransportAcceptListener;
import chat.transport.TransportFactory;
import chat.transport.ServerTransport;
import lombok.extern.slf4j.Slf4j;

import javax.net.ServerSocketFactory;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

@Slf4j
public class TcpServerTransport implements ServerTransport, Runnable {

    private final int port;
    private final int backlog;
    private final ServerSocketFactory serverSocketFactory;
    private final TransportFactory transportFactory;

    private TransportAcceptListener transportAcceptListener;

    private ServerSocket serverSocket;

    TcpServerTransport(int port,
                       int backlog,
                       ServerSocketFactory serverSocketFactory,
                       TransportFactory transportFactory) {
        this.port = port;
        this.backlog = backlog;
        this.serverSocketFactory = serverSocketFactory;
        this.transportFactory = transportFactory;
    }

    @Override
    public void setTransportAcceptListener(TransportAcceptListener transportAcceptListener) {
        this.transportAcceptListener = transportAcceptListener;
    }

    @Override
    public void start() throws Exception {
        log.info("Starting server...");

        serverSocket = serverSocketFactory.createServerSocket(port, backlog);

        new Thread(this).start();
    }

    @Override
    public void stop() throws Exception {
        // TODO stop
    }

    @Override
    public void run() {
        log.debug("Server started on port: {}", port);

        // TODO isRunning
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                log.debug("Handle accept Socket: {}", socket);
                transportAcceptListener.onAccept(transportFactory.createTransport(socket));
            } catch (IOException e) {
                transportAcceptListener.onAcceptError(e);
            }
        }
    }
}