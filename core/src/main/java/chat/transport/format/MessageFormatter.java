package chat.transport.format;

import chat.message.Message;

// TODO Переписать
public interface MessageFormatter {

    byte[] serialize(Message message);

    Message deserialize(byte[] bytes);
}
