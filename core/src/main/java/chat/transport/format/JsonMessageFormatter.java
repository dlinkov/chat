package chat.transport.format;

import chat.message.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonMessageFormatter implements MessageFormatter {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public byte[] serialize(Message message) {
        try {
            return (objectMapper.writeValueAsString(message) + '\n').getBytes();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Message deserialize(byte[] bytes) {
        try {
            return objectMapper.readValue(bytes, Message.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
