package chat.transport;

import java.net.Socket;

public interface TransportFactory {

    ServerTransport createServerTransport(int port, int backlog);

    Transport createTransport(Socket socket);
}
