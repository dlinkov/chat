package chat.transport;

public interface TransportAcceptListener {

    void onAccept(Transport transport);

    void onAcceptError(Exception e);
}
