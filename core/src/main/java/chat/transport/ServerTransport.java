package chat.transport;

import chat.Service;

public interface ServerTransport extends Service {

    void setTransportAcceptListener(TransportAcceptListener transportAcceptListener);
}