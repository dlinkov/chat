package chat.transport;

import chat.Service;
import chat.message.Message;

public interface Transport extends Service {

    String getRemoteAddress();

    void request(Message message);

    void setMessageListener(MessageListener messageListener);
}
